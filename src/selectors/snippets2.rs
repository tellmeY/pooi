use comfy_table::{
    modifiers::{UTF8_ROUND_CORNERS, UTF8_SOLID_INNER_BORDERS},
    presets::UTF8_FULL,
    Attribute, Cell, CellAlignment, Color, ContentArrangement, Row, Table
};
use scraper::{Html, Selector};

pub fn main(data: &Html, tty: &bool, w: usize) {
    // Extract the title if it exists
    let title_selector = Selector::parse("div.co8aDb").unwrap();
    let title = data
        .select(&title_selector)
        .next()
        .map(|elem| elem.text().collect::<Vec<&str>>().join(""))
        .unwrap_or_default();

    // Extract the list items
    let list_selector = Selector::parse("li.TrT0Xe").unwrap();
    let list: Vec<String> = data
        .select(&list_selector)
        .map(|elem| elem.text().collect::<Vec<&str>>().join(""))
        .collect();

    match tty {
        true => {
            let mut t = Table::new();
            t.load_preset(UTF8_FULL);
            t.apply_modifier(UTF8_ROUND_CORNERS);
            t.apply_modifier(UTF8_SOLID_INNER_BORDERS);
            t.set_content_arrangement(ContentArrangement::Dynamic);

            if w >= 100 {
                t.set_width(100);
            }

            if !title.is_empty() {
                t.set_header(Row::from(vec![
                    Cell::new(&title)
                        .set_alignment(CellAlignment::Center)
                        .add_attribute(Attribute::Bold)
                        .fg(Color::Green),
                ]));
            }

            for item in list {
                t.add_row(Row::from(vec![
                    Cell::new(item)
                        .set_alignment(CellAlignment::Center)
                        .add_attribute(Attribute::Bold),
                ]));
            }

            println!("{}", t);
        }
        false => {
            if !title.is_empty() {
                println!("{}\n", title);
            }

            for (i, item) in list.iter().enumerate() {
                println!("[{}] {}", i + 1, item);
            }
        }
    }
}
