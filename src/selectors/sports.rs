use comfy_table::{
    modifiers::{UTF8_ROUND_CORNERS, UTF8_SOLID_INNER_BORDERS},
    presets::{UTF8_BORDERS_ONLY, UTF8_FULL},
    Attribute, Cell, CellAlignment, Color, ContentArrangement, Row, Table,
};
use scraper::Html;

use crate::selectors::get_vec;

pub fn main(data: &Html, tty: &bool, w: usize) {
    let heading = get_vec(data, "div.imso_mh__stts-l.imso-ani.imso_mh__stts-l-cont");
    let scores = get_vec(data, "div.imso_mh__scr-sep");
    let team1 = get_vec(data, "div.imso_mh__first-tn-ed.imso_mh__tnal-cont.imso-tnol");
    let team2 = get_vec(data, "div.imso_mh__second-tn-ed.imso_mh__tnal-cont.imso-tnol");

    // Convert Vec<&str> to Vec<String> for compatibility
    let heading: Vec<String> = heading.iter().map(|&s| s.to_string()).collect();
    let scores: Vec<String> = scores.iter().map(|&s| s.to_string()).collect();
    let team1: Vec<String> = team1.iter().map(|&s| s.to_string()).collect();
    let team2: Vec<String> = team2.iter().map(|&s| s.to_string()).collect();

    match heading.len() {
        3 => display_three_headings(&heading, &scores, &team1, &team2, *tty, w),
        4 => display_four_headings(&heading, &scores, &team1, &team2, *tty, w),
        _ => panic!("sports: heading.len() != 3||4; result cannot be formatted"),
    }
}

fn display_three_headings(
    heading: &[String],
    scores: &[String],
    team1: &[String],
    team2: &[String],
    tty: bool,
    w: usize,
) {
    if tty {
        let mut t = Table::new();
        t.load_preset(UTF8_BORDERS_ONLY);
        t.apply_modifier(UTF8_ROUND_CORNERS);
        t.set_content_arrangement(ContentArrangement::Dynamic);
        if w >= 100 {
            t.set_width(100);
        }
        t.set_header(Row::from(vec![
            Cell::new(&heading[0]).set_alignment(CellAlignment::Center),
            Cell::new(&heading[1])
                .set_alignment(CellAlignment::Center)
                .fg(Color::DarkGrey),
            Cell::new(&heading[2])
                .set_alignment(CellAlignment::Center)
                .add_attribute(Attribute::Bold)
                .fg(Color::Magenta),
        ]));
        t.add_row(Row::from(vec![
            Cell::new(&team1[0])
                .set_alignment(CellAlignment::Center)
                .add_attribute(Attribute::Bold),
            Cell::new(&scores[0])
                .set_alignment(CellAlignment::Center)
                .fg(Color::DarkGrey),
            Cell::new(&team2[0])
                .set_alignment(CellAlignment::Center)
                .add_attribute(Attribute::Bold),
        ]));
        println!("{}", t);
    } else {
        println!(
            "{}\n{} {} {}",
            heading.join(" "),
            team1[0], scores[0], team2[0]
        );
    }
}

fn display_four_headings(
    heading: &[String],
    scores: &[String],
    team1: &[String],
    team2: &[String],
    tty: bool,
    w: usize,
) {
    if tty {
        let mut t = Table::new();
        t.load_preset(UTF8_FULL);
        t.apply_modifier(UTF8_ROUND_CORNERS);
        t.apply_modifier(UTF8_SOLID_INNER_BORDERS);
        t.set_content_arrangement(ContentArrangement::Dynamic);
        if w > 100 {
            t.set_width(100);
        }
        let header_text = heading[1..4].join("");
        t.set_header(Row::from(vec![
            Cell::new(header_text)
                .set_alignment(CellAlignment::Center)
                .fg(Color::DarkGrey),
            Cell::new(&heading[0])
                .set_alignment(CellAlignment::Center)
                .add_attribute(Attribute::Bold)
                .fg(Color::Green),
        ]));
        let a = scores[0].parse::<u32>().unwrap();
        let b = scores[2].parse::<u32>().unwrap();
        if a == b {
            t.add_row(create_row(&team1.join("\n"), &scores[0], None));
            t.add_row(create_row(&team2.join("\n"), &scores[2], None));
        } else if a > b {
            t.add_row(create_row(&team1.join("\n"), &scores[0], Some(Color::Yellow)));
            t.add_row(create_row(&team2.join("\n"), &scores[2], None));
        } else {
            t.add_row(create_row(&team1.join("\n"), &scores[0], None));
            t.add_row(create_row(&team2.join("\n"), &scores[2], Some(Color::Yellow)));
        }
        println!("{}", t);
    } else {
        println!(
            "{}{}{} | {}\n{} {}\n{} {}",
            heading[1], heading[2], heading[3], heading[0],
            team1.join(" "), scores[0],
            team2.join(" "), scores[2]
        );
    }
}

fn create_row(team: &str, score: &str, color: Option<Color>) -> Row {
    match color {
        Some(c) => Row::from(vec![
            Cell::new(team)
                .set_alignment(CellAlignment::Center)
                .add_attribute(Attribute::Bold)
                .fg(c),
            Cell::new(score)
                .set_alignment(CellAlignment::Center)
                .add_attribute(Attribute::Bold)
                .fg(c),
        ]),
        None => Row::from(vec![
            Cell::new(team)
                .set_alignment(CellAlignment::Center)
                .add_attribute(Attribute::Bold),
            Cell::new(score)
                .set_alignment(CellAlignment::Center)
                .add_attribute(Attribute::Bold),
        ]),
    }
}
