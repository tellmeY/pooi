use super::{get_vec,default_output};
use scraper::Html;

pub fn main(data: &Html, tty: &bool, w: usize) {
    // Extracting lyrics from the HTML
    let mut lyrics = get_vec(data, "div.i4J0ge");

    // Removing the last four elements (e.g., metadata or irrelevant info)
    for _ in 0..4 {
        lyrics.pop();
    }

    // Extracting duplicate lines which need to be removed
    let duplicate_lines = get_vec(data, "div.ujudUb.WRZytc.OULBYb");
    let dups_len = duplicate_lines.len();

    // Finding the index of the last duplicate line in the lyrics
    let mut lyrics_index = lyrics
        .iter()
        .position(|r| r == &duplicate_lines[dups_len - 1])
        .unwrap();

    // Removing all duplicate lines from the lyrics
    for _ in 0..dups_len {
        lyrics.remove(lyrics_index);
        if lyrics_index > 0 {
            lyrics_index -= 1;
        }
    }

    // Output based on TTY flag
    match tty {
        true => default_output(&lyrics.join("\n"), w),
        false => println!("{}", lyrics.join("\n")),
    }
}